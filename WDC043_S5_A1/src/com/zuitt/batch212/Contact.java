package com.zuitt.batch212;

import java.util.ArrayList;

public class Contact {

    // Properties
    String name;
    ArrayList<String> numbers = new ArrayList<>();
    ArrayList<String> addresses = new ArrayList<>();


    // Empty Constructor
    public Contact(){};

    // Parameterized Constructor
    public Contact (String name, String number, String address) {
        this.name = name;
        this.numbers.add(number);
        this.addresses.add(address);
    }


    // Getters
    public String getName() {
        return this.name;
    }
    public ArrayList<String> getNumbers() {
        return this.numbers;
    }
    public ArrayList<String> getAddresses() {
        return this.addresses;
    }

    // Setters
    public void setName(String name) {
        this.name = name;
    }
    public void setNumbers(String number) {
        this.numbers.add(number);
    }
    public void setAddresses(String address) {
        this.addresses.add(address);
    }


}
