package com.zuitt.batch212;

import java.util.ArrayList;
import java.util.Objects;

public class Phonebook {

    // Property
    ArrayList<Contact> contacts = new ArrayList<>();


    // Empty Constructor
    public Phonebook(){};

    // Parameterized Constructor
    public Phonebook (Contact contact) {
        this.contacts.add(contact);
    }


    // Getter
    public ArrayList<Contact> getContacts() {
        return this.contacts;
    }

    // Setter
    public void setContacts(Contact contact) {
        this.contacts.add(contact);
    }

    // Print Method
    public void printPhoneBook() {

        String output = "";

        if (this.contacts.size() == 0) output = "Phonebook is empty!";

        else {

            for (Contact contact: contacts) {

                // Insert a new line for succeeding contacts after the first one
                output +=
                        output.length() == 0
                            ? "" : "\n";

                // Contact Name
                output += "\n" + contact.getName() + "\n" + "-".repeat(50);


                // Contact Numbers
                output += "\n" + contact.getName() + " has the following registered numbers:";

                if (contact.getNumbers().size() == 0)
                    output += "\n" + "# No registered numbers #";
                else {
                    for (String number: contact.getNumbers()) {
                        output += "\n" + number;
                    }
                }

                output += "\n" + "-".repeat(50);


                // Contact Addresses
                output += "\n" + contact.getName() + " has the following registered addresses:";

                if (contact.getAddresses().size() == 0)
                    output += "\n" + "# No registered address #";
                else {
                    for (String address: contact.getAddresses()) {
                        output += "\n" + address;
                    }
                }

                output += "\n" + "=".repeat(50);

            }

        }

        System.out.println(output);
    }
}
